#!/usr/bin/env python3

import collections
import requests
import os 
import sys 
import importlib

#Globals 
FNAME = 'food_data.json'
PROTEIN = False
FAT = False
CARB = False
SUGAR = False 
NAME = ''

# usage function that explains to the user how to work the program 
def usage(status = 0): 
	''' Display usage information and exit with specified status '''
	print('''Usage: Nutrition Specs

    -p search for protein
	-f search for fat 
	-c search for carbohydrates
	-s search for sugar 
	
	Follow flag with two integers, representing the range to search low to high 
	'''
	)
	sys.exit(status)


if __name__ == '__main__': 

	args = sys.argv[1:]

	# error if no args entered or 
	# error if num of args is not divisible by 3,
	# because args should be label, then two ints for range - 3 total 
	if len(args) == 0 or len(args) % 3 != 0: 
		usage(0)

	# Read in args
	while len(args): 
		arg = args.pop(0)

		if arg == '-p':
			PROTEIN = True
			arg = args.pop(0)
			pLow = float(arg)
			arg = args.pop(0)
			pHigh = float(arg)
		elif arg == '-f':
			FAT = True
			arg = args.pop(0)
			fLow = float(arg)
			arg = args.pop(0)
			fHigh = float(arg)
		elif arg == '-c':
			CARB = True
			arg = args.pop(0)
			cLow = float(arg)
			arg = args.pop(0)
			cHigh = float(arg)
		elif arg == '-s':
			SUGAR = True
			arg = args.pop(0)
			sLow = float(arg)
			arg = args.pop(0)
			sHigh = float(arg)
		else:
			usage(1)

	# Find and print food items that match args 
	with open(FNAME, 'r') as f: 
		read_data = f.read() # type: string

	# initialize all variables 
	# these variables determine what current nutrient is 
	isProtein = False
	isFat = False 
	isCarb = False 
	isSugar = False

	# these variables determine if current nutrient fits given value range
	pStatus = 0
	fStatus = 0
	cStatus = 0
	sStatus = 0

	# these variables determine if the food's name should be printed 
	pFinal = 0
	fFinal = 0
	cFinal = 0
	sFinal = 0

	for line in read_data.splitlines(): #splits str into smaller str by line
		
		# status = 0 is default
		# status = 1 means do not print
		# status = 2 means print 

		# status = 0
		temp = line.strip()
		
		# when there is a new food name, reset vars 
		if temp[1:5] == 'name':
			NAME = temp[9:-2]
			pStatus = 0
			fStatus = 0
			cStatus = 0
			sStatus = 0
			pFinal = 0
			fFinal = 0
			cFinal = 0
			sFinal = 0

		# only runs if -p flag was given 
		if PROTEIN:
			# checks for nutrient 
			if temp[0:10] == "\"nutrient\"":
				# checks if nutrient is Protein 
				if temp[13:-2] == "Protein":
					isProtein = True
				else:
					isProtein = False
			# if nutrient is protein 
			if isProtein: 
				# looks for the value 
				if temp[1:6] == 'value':
					# if no value given, assigns a val of 0
					if temp[10:-2] == '--':
						val = 0.0
					else:
						val = float(temp[10:-2])
					# status set to 1 if val out of range 
					if val > pHigh or val < pLow:
						pStatus = 1
					# otherwise, unless val was previously out of 
					# bounds, set status to 2 
					else:
						if pStatus != 1:
							pStatus = 2

		# performs the same checks as protein on fat 
		if FAT: 
			if temp[0:10] == "\"nutrient\"":
				if temp[13:-2] == "Total lipid (fat)":
					isFat = True
				else:
					isFat = False

			if isFat:
				if temp[1:6] == 'value':
					if temp[10:-2] == '--':
						val = 0.0
					else:
						val = float(temp[10:-2])

					if val > fHigh or val < fLow:
						fStatus = 1
					else:
						if fStatus != 1:
							fStatus = 2

		# performs the same checks as protein on carbs
		if CARB: 
			if temp[0:10] == "\"nutrient\"":
				if temp[13:-2] == "Carbohydrate, by difference":
					isCarb = True
				else:
					isCarb = False
				
			if isCarb:
				if temp[1:6] == 'value':
					if temp[10:-2] == "--":
						val = 0.0
						
					else:
						val = float(temp[10:-2])

					if val > cHigh or val < cLow:
						cStatus = 1
					else: 
						if cStatus != 1:
							cStatus = 2

				
		# performs the same checks as protein on sugar 
		if SUGAR:
			if temp[0:10] == "\"nutrient\"":
				if temp[13:-2] == "Sugars, total": 
					isSugar = True
				else:
					isSugar = False 

			if isSugar:
				if temp[1:6] == "value":
					if temp[10:-2] == "--":
						val = 0.0
					else:
						val = float(temp[10:-2])

					if val > sHigh or val < sLow:
						sStatus = 1
					else:
						if sStatus != 1:
							sStatus = 2


		# determines if name should be printed
		if PROTEIN and pStatus != 2:
			pFinal = 1 # name should not be printed 
		else: 
			pFinal = 2 # name should be printed 

		if FAT and fStatus != 2:
			fFinal = 1
		else: 
			fFinal =2 

		if CARB and cStatus != 2:
			cFinal = 1
		else: 
			cFinal = 2

		if SUGAR and sStatus != 2:
			sFinal = 1 
		else: 
			sFinal = 2 

		# if correct conditions were met, print out name and reset vars
		if pFinal != 1 and fFinal != 1 and cFinal != 1 and sFinal != 1:
			print(NAME)
			pStatus = 1
			fStatus = 1
			cStatus = 1
			sStatus = 1